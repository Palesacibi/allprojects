
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Administrator on 10/7/2015.
 */
public class ReadXMLFile
{
    public  static void main(String[] args)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();


        try{

            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File("student.xml ");
            Document document =builder.parse(file);

            ArrayList<Student> studentList = new ArrayList<Student>();

            NodeList nodeList = document.getDocumentElement().getChildNodes();

            for (int i = 0; i < nodeList.getLength(); i++) {

                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element elem = (Element) node;

                    String name = node.getAttributes().getNamedItem("Name").getNodeValue();
                    String gender = node.getAttributes().getNamedItem("Gender").getNodeValue();
                    String dateOfBirth = node.getAttributes().getNamedItem("DateOfBirth").getNodeValue();
                    // Pet pet = new Pet("None", "None");
                    // System.out.println(pet.getName());

                    NodeList childList=((Element) node).getElementsByTagName("Pet");
                    //   System.out.println(childList.getLength());
                    if(childList.getLength()>0){

                        String petType = childList.item(0).getAttributes().getNamedItem("Type").getNodeValue();
                        String petName = childList.item(0).getAttributes().getNamedItem("Name").getNodeValue();
                        /*System.out.println(petName);
                        System.out.println(petType);*/

                        Pet pet = new Pet(petName,petType);
                    }

                    // Student student = new Student(name,gender,dateOfBirth,pet);

                    System.out.println(name);


                }

            }
        }
        catch (Exception e){
            System.out.println("Error is = "+e.getMessage());
        }
    }
}
