import java.util.Scanner;

/**
 * Created by Administrator on 10/7/2015.
 */
public class CallingMethods {
    public static void main(String args[]) {

        /** Calling a method with a fixed number of arguments
         *Calling a method with optional arguments
         *Calling a method with a variable number of arguments
         **/


        CallingMethods Puppy = new CallingMethods();

        int Solution = Puppy.BMI(10,30,0);


        Puppy.bark();
        int value = Puppy.BMI(10,9,0);
        System.out.println(Solution);
        int value2= Puppy.BMI(2,9,0);
        System.out.println(value2);
        System.out.println(value);
        Puppy.getPuppyColor();
        Puppy.getPuppyBreed();

    }

    //Calling a private method
    private int Height;
    private int Weight;
    private String PuppyColor;
    private String PuppyBreed;
    //private String PuppyAge;


    //Calling a method that requires no arguments
    CallingMethods() {


    }

    //Arguments passed by reference
    public String getPuppyColor() {
        String PuppyColor = "Black";
        System.out.println("The color of the puppy is : " + PuppyColor);
        return PuppyColor;
    }


    //Arguments passed by value
    public void getPuppyBreed() {
        String PuppyBreed = "German Sherperd";
        System.out.println("The breed of the puppy is : " + PuppyBreed);


    }

    //protected method
    protected int BMI(int Height, int Weight, int BMI) {
        this.Height = Height;
        this.Weight = Weight;

        BMI = Height * Weight;

        return BMI;
    }

    //Public method
    public void bark() {
        System.out.println("Woof");
    }



}
