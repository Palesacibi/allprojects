import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Administrator on 10/7/2015.
 */
public class RomanNumerals
{
    public static void main(String[] args) {
        String romanInput1 = JOptionPane.showInputDialog(null,"Insert roman number ");
        String romanInput2 = JOptionPane.showInputDialog(null,"Insert roman number ");


        System.out.println("The first number is "+ getIntnum(romanInput1));
        System.out.println("The first number is "+ getIntnum(romanInput2));

        System.out.println("The sum of the two numbers is = "+ add_roman(romanInput1,romanInput2));

    }

    public static int add_roman(String roman1, String roman2){
        int answer=0;

        int num1=getIntnum(roman1);
        int num2=getIntnum(roman2);



        return num1+num2;
    }

    public static int getIntnum(String roman){
        int number=0;
        ArrayList<Integer> Rn = new ArrayList<>();
        for (int i = 0; i <roman.length() ; i++) {

            if(roman.charAt(i)=='M'){
                Rn.add(1000);
            }
            else if(roman.charAt(i)=='D'){
                Rn.add(500);
            }
            else if(roman.charAt(i)=='C'){
                Rn.add(100);
            }
            else if(roman.charAt(i)=='L'){
                Rn.add(50);
            }
            else if(roman.charAt(i)=='X'){
                Rn.add(10);
            }
            else if(roman.charAt(i)=='V'){
                Rn.add(5);
            }
            else if(roman.charAt(i)=='I'){
                Rn.add(1);
            }

        }

        for (int i = 0; i <Rn.size()-1 ; i++) {
            if(Rn.get(i)<Rn.get(i+1)){
                number +=Rn.get(i+1)-Rn.get(i);
                i++;
            }
            else{
                number+= Rn.get(i);
            }
        }
        return  number;
    }
}
