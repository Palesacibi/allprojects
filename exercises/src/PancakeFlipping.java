/**
 * Created by Administrator on 10/7/2015.
 */
public class PancakeFlipping {
    public static void main(String[] args)
    {
        //Numbers which need to be sorted
        int Pancakes[] = {23,5,23,1,7,12,3,34,0};

        //Displaying the numbers before sorting
        System.out.print("Before sorting, Pancakes order is ");
        for(int i = 0; i < Pancakes.length; i++)
        {
            System.out.print(Pancakes[i]+" ");
        }
        System.out.println();

        //Sorting in ascending order using bubble sort
        bubbleSortInAscendingOrder(Pancakes);

        //Displaying the pancakes after sorting
        System.out.print("After flipping the pancakes, order is ");
        for(int i = 0; i < Pancakes.length; i++)
        {
            System.out.print(Pancakes[i]+" ");
        }

    }

    //This method sorts the input array in asecnding order

    public static void bubbleSortInAscendingOrder( int numbers[])
    {
        int temp;

        for(int i = 0; i < numbers.length; i++)
        {
            for(int j = 1; j < (numbers.length -i); j++)
            {
                //if numbers[j-1] > numbers[j], swap the elements
                if(numbers[j-1] > numbers[j])
                {
                    temp = numbers[j-1];
                    numbers[j-1]=numbers[j];
                    numbers[j]=temp;
                }
            }
        }
    }
}
