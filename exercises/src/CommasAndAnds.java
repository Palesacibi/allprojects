import javax.swing.*;

/**
 * Created by Administrator on 10/7/2015.
 */
public class CommasAndAnds {
    public static void main(String args[])
    {
        String braceOpen ="{";
        String braceClose ="}";
        String textInput = JOptionPane.showInputDialog(null, "Enter the text :");
        if (textInput.isEmpty())
        {
            System.out.println(braceOpen+braceClose);
        }
        else{
            System.out.print(braceOpen + textInput.replace(",", " and "));
        }

        System.out.println(braceClose);

    }
}
