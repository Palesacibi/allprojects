import java.util.ArrayList;


/**
 * Created by Administrator on 10/8/2015.
 */
public class RingOfDeath {
    public static int execute(int n, int m){
        int NoOfKilled = 0;
        ArrayList<Integer> prisoners = new ArrayList<Integer>(n);
        for(int i = 0;i < n;i++){
            prisoners.add(i);
        }
        System.out.println("Prisoners killed in order:");
        while(prisoners.size() > 1){
            NoOfKilled  = (NoOfKilled + n - 1) % prisoners.size();
            System.out.print(prisoners.get(NoOfKilled ) + " ");
            prisoners.remove(NoOfKilled );
        }
        System.out.println();
        return prisoners.get(0);}


    public static ArrayList<Integer> executeAllButM(int n,  int m){
        int NoOfKilled = 0;
       ArrayList<Integer> prisoners = new ArrayList<Integer>(n);
       for(int i = 0;i < n;i++){
            prisoners.add(i);
       }
        System.out.println("Prisoners killed in order:");
        while(prisoners.size() > m){
            NoOfKilled = (NoOfKilled+ m - 1) % prisoners.size();
            System.out.print(prisoners.get(NoOfKilled) + " ");
            prisoners.remove(NoOfKilled);
       }
        System.out.println();
        return prisoners;
    }

    public static void main(String[] args){
        System.out.println("Survivor: " + execute(41, 3));
        //System.out.println("Survivors: " + executeAllButM(41, 3, 3));
    }
}

