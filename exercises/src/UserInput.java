import java.util.Scanner;

/**
 * Created by Administrator on 10/7/2015.
 */
public class UserInput {
    public static void main(String[] args) {
        String name;
        long UserInput;
        Scanner in = new Scanner(System.in);

        // Reads a single line from the console
        // and stores into name variable
        name = in.nextLine();

        // Reads a integer from the console
        // and stores into age variable
        UserInput=in.nextInt();
        in.close();

        // Prints name and age to the console
        System.out.println("Name :"+name);
        System.out.println("UserInput :"+UserInput);

    }
}
